# Library

This is a collection of patterns that can be reused throughout other projects.  
It was developed in Ada, for other Ada projects.

## State Machine

A design pattern for system requirements that are presented as a state machine.

## Basic List

A basic doubly linked ist.

## XYZ Filter 

A filter that toggles state based on defined threshholds.

## Hash Chain

A List where each node is dependent its the previous (parent) node.
