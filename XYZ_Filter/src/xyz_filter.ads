package xyz_filter is

  type Filter_Type is private;
  
  function Create (Min             : Natural;
                   Max             : Natural;
                   Increment_Value : Natural := 1;
                   Decrement_Value : Natural := 1) return Filter_Type;
  --
  -- Creates a new XYZ Filter
  
  procedure Increment (Filter : in out Filter_Type);
  --
  -- Increments the Counter
  
  procedure Decrement (Filter : in out Filter_Type);
  --
  -- Decrements the Counter
  
  function Threshold_Reached (Filter : in Filter_Type) return Boolean;
  --
  -- Returns True if the Threshold has been reached
  
private
  
  type Filter_Type is record
    Min               : Natural;
    Max               : Natural;
    Current_Value     : Natural;
    Increment_Value   : Natural;
    Decrement_Value   : Natural;
    Threshold_Reached : Boolean;
  end record;

end xyz_filter;
