package body xyz_filter is

  ------------
  -- Create --
  ------------

  function Create (Min             : Natural;
                   Max             : Natural;
                   Increment_Value : Natural := 1;
                   Decrement_Value : Natural := 1) return Filter_Type is
    Filter : Filter_Type;
   begin
    Filter := (Min               => Min,
               Max               => Max,
               Current_Value     => Min,
               Increment_Value   => Increment_Value,
               Decrement_Value   => Decrement_Value,
               Threshold_Reached => False);
    return Filter;
   end Create;

  ---------------
  -- Increment --
  ---------------

  procedure Increment (Filter : in out Filter_Type) is
  begin
    Filter.Current_Value :=
      Natural'Min (Filter.Current_Value + Filter.Increment_Value, Filter.Max);
    Filter.Threshold_Reached :=
      Filter.Current_Value = Filter.Max or Filter.Threshold_Reached;
  end Increment;

  ---------------
  -- Decrement --
  ---------------

  procedure Decrement (Filter : in out Filter_Type) is
  begin
    Filter.Current_Value :=
      Natural'Max (Filter.Current_Value - Filter.Decrement_Value, Filter.Min);
    Filter.Threshold_Reached :=
      Filter.Current_Value = Filter.Min and not Filter.Threshold_Reached;
  end Decrement;

  -----------------------
  -- Threshold_Reached --
  -----------------------

  function Threshold_Reached (Filter : in Filter_Type) return Boolean is
  begin
    return Filter.Threshold_Reached;
  end Threshold_Reached;

end xyz_filter;
