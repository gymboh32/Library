package Trust is

  type Trust_Type is private;
  
  procedure Add_Trust (Object : in out Trust_Type);
  --
  -- Adds a Level of Trust from the Object
  
  procedure Remove_Trust (Object : in out Trust_Type);
  --
  -- Removes a Level of Trust from the Object
  
  function Level (Object : in Trust_Type) return Float;
  --
  -- Returns the Level of Trust in the Object
  
private
  
  type Trust_Type is record
    Trust        : Natural;
    Distrust     : Natural;
    Wilson_Score : Float;
  end record;
  
end Trust;
