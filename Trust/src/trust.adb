with Ada.Numerics.Generic_Elementary_Functions;

package body Trust is

  -----------
  -- LOCAL --
  -----------

  ------------------
  -- Update_Score --
  ------------------

  procedure Update_Score (Object : in out Trust_Type) is
    Z             : Float   := 1.61803; -- Arbitrarily chosen to be fibonacci golden ratio
    P             : Float   := 0.0;
    Total_Trust   : Natural := 0;
    Left          : Float   := 0.0;
    Right         : Float   := 0.0;
    Under         : Float   := 0.0;
    Success       : Boolean := False;
    package Math is new Ada.Numerics.Generic_Elementary_Functions
      (Float_Type => Float);
  begin

    Total_Trust := Object.Trust + Object.Distrust;
    P := Float (Object.Trust) / Float (Total_Trust);

    Left := P + 1.0 / (2.0 * Float (Total_Trust)) * Z * Z;
    Right := Z * Math.Sqrt (P * (1.0 - P) /
                              Float (Total_Trust) + Z * Z /
                            (4.0 * Float (Total_Trust * Total_Trust)));
    Under := 1.0 + 1.0 / Float (Total_Trust) * Z * Z;

    Object.Wilson_Score := (Left - Right) / Under;

  end Update_Score;

  ------------
  -- PUBLIC --
  ------------

  ---------------
  -- Add_Trust --
  ---------------

  procedure Add_Trust (Object : in out Trust_Type) is
  begin
    Object.Trust := Object.Trust + 1;
    Update_Score (Object);
  end Add_Trust;

  ------------------
  -- Remove_Trust --
  ------------------

  procedure Remove_Trust (Object : in out Trust_Type) is
  begin
    Object.Distrust := Object.Distrust + 1;
    Update_Score (Object);
  end Remove_Trust;

  -----------
  -- Level --
  -----------

  function Level (Object : in Trust_Type) return Float is
  begin
    return Object.Wilson_Score;
  end Level;

end Trust;
