with Ada.Text_IO; use Ada.Text_IO;
with Thermostat;

procedure Main is
  T_1 : Thermostat.Thermostat_Type;
begin
  T_1 := Thermostat.Create;
  Thermostat.Run (T_1);
  Put_Line ("Hello, World!");
end Main;
