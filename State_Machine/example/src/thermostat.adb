pragma Ada_2012;

with Thermostat.Monitor;
with State;

package body Thermostat is

  ------------
  -- Create --
  ------------

  function Create return Thermostat_Type is
    T_1 : Thermostat_Type;
  begin
    T_1.Current_Temp := 0;
    T_1.Target_Temp := 0;
    return T_1;
  end Create;

  --------------
  -- Set_Temp --
  --------------

  procedure Set_Temp
    (Thermostat  : in out Thermostat_Type;
     Target_Temp : in     Temperature_Type) is
  begin
    Thermostat.Target_Temp := Target_Temp;
  end Set_Temp;

  --------------
  -- Get_Temp --
  --------------

  function Get_Temp (Thermostat : Thermostat_Type) return Temperature_Type is
  begin
    return Thermostat.Current_Temp;
  end Get_Temp;

  ---------
  -- Run --
  ---------

  procedure Run (Thermostat : in out Thermostat_Type) is
    Monitor_State : State.State_Type := Monitor.Init;
  begin
    Self := Thermostat;
    State.Run (Monitor_State);
  end Run;

end Thermostat;
