with State; use State;

package Thermostat.Monitor is

  function Init return State_Type;
  --
  -- Initialize the Monitor State
  
  function On_Execute return Boolean;
  --
  -- function to be run executing the Monitor State
  
  function On_Exit return State_Type;
  --
  -- Function to be run before Exiting the Monitor State
  -- Returns Heat if Temp is too Low
  -- Returns Cool if Temp is too High

end Thermostat.Monitor;
