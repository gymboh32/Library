package Thermostat is

  type Thermostat_Type is private;
  subtype Temperature_Type is Integer range -100 .. 100;

  function Create return Thermostat_Type;
  --
  -- Create a new Thermostat
  
  procedure Set_Temp (Thermostat  : in out Thermostat_Type;
                      Target_Temp : in     Temperature_Type);
  --
  -- Sets the Target Temperature
  
  function Get_Temp (Thermostat : Thermostat_Type) return Temperature_Type;
  --
  -- Gets the Current Temperature
  
  procedure Run (Thermostat : in out Thermostat_Type);
  --
  -- Run the Thermostat state machine
  
private
  
  type Thermostat_Type is record
    Current_Temp : Temperature_Type;
    Target_Temp  : Temperature_Type;
  end record;
  
  Self : Thermostat_Type;  

end Thermostat;
