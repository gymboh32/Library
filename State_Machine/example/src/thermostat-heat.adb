pragma Ada_2012;

with Ada.Text_IO; use Ada.Text_IO;
with Thermostat.Monitor;

package body Thermostat.Heat is

  ----------
  -- Init --
  ----------

  function Init return State_Type is
    S : State_Type := Create;
  begin
    Register_Execute (S, On_Execute'Access);
    Register_Exit (S, On_Exit'Access);
    return S;
  end Init;

  ----------------
  -- On_Execute --
  ----------------

  function On_Execute return Boolean is
  begin
    Put_Line ("Thermostat - Heat");
    Put_Line ("Current: " & Self.Current_Temp'Img);
    Put_Line ("Target : " & Self.Target_Temp'Img);
    delay 2.0;

    Self.Current_Temp := Temperature_Type'Min (Self.Current_Temp + 3,
                                               Temperature_Type'Last);
    return True;
  end On_Execute;

  -------------
  -- On_Exit --
  -------------

  function On_Exit return State_Type is
    Next : State_Type := Monitor.Init;
  begin
    return Next;
  end On_Exit;

end Thermostat.Heat;
