with State; use State;

package Thermostat.Cool is
   
  function Init return State_Type;
  --
  -- Initialize the State
  
  function On_Execute return Boolean;
  --
  -- function to be run executing the State
  
  function On_Exit return State_Type;
  --
  -- Function to be run before Exiting the State
  -- Returns to Monitor State

end Thermostat.Cool;
