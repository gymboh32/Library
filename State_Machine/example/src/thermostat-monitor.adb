pragma Ada_2012;
with Ada.Text_IO; use Ada.Text_IO;
with Thermostat.Heat;
with Thermostat.Cool;

package body Thermostat.Monitor is

  ----------
  -- Init --
  ----------

  function Init return State_Type is
    S : State_Type := Create;
  begin
    Register_Execute (S, On_Execute'Access);
    Register_Exit (S, On_Exit'Access);
    return S;
  end Init;

  ----------------
  -- On_Execute --
  ----------------

  function On_Execute return Boolean is
  begin
    Put_Line ("Thermostat - Monitor");
    Put_LIne ("Current: " & Self.Current_Temp'Img);
    Put_Line ("Target : " & Self.Target_Temp'Img);
    delay 2.0;
    Self.Current_Temp := Self.Current_Temp - 1;
    return True;
  end On_Execute;

  -------------
  -- On_Exit --
  -------------

  function On_Exit return State_Type is
    Next : State_Type := Final_State;
    Offset : constant := 5;
  begin
    if Self.Current_Temp < Self.Target_Temp + Offset then
      Next := Heat.Init;
    else
      Next := Cool.Init;
    end if;
    return Next;
  end On_Exit;

end Thermostat.Monitor;
