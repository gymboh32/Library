pragma Ada_2012;

with Ada.Numerics.Discrete_Random; use Ada.Numerics;

package body State is

  ------------
  -- Create --
  ------------

  function Create return State_Type is
    package Rand_Id is new Discrete_Random (Id_Type);
    Gen       : Rand_Id.Generator;
    New_State : State_Type;
  begin
    Rand_Id.Reset (Gen);

    New_State.Id := Rand_Id.Random (Gen);
    New_State.On_Entry := Null_Entry'Access;
    New_State.On_Execute := Null_Execute'Access;
    New_State.On_Exit := Null_Exit'Access;

    return New_State;
  end Create;

  --------------------
  -- Register_Entry --
  --------------------

  procedure Register_Entry
    (State    : in out State_Type;
     On_Entry : in     Access_Entry_Type) is
  begin
    State.On_Entry := On_Entry;
  end Register_Entry;

  ----------------------
  -- Register_Execute --
  ----------------------

  procedure Register_Execute
    (State      : in out State_Type;
     On_Execute : in     Access_Execute_Type) is
  begin
    State.On_Execute := On_Execute;
  end Register_Execute;

  -------------------
  -- Register_Exit --
  -------------------

  procedure Register_Exit
    (State   : in out State_Type;
     On_Exit : in     Access_Exit_Type) is
  begin
    State.On_Exit := On_Exit;
  end Register_Exit;

  ---------
  -- Run --
  ---------

  function Run (State : State_Type) return State_Type is
    Next : State_Type := Final_State;
  begin
    if State.Id in Id_Type then
      if State.On_Entry.all then
        if State.On_Execute.all then
          Next := State.On_Exit.all;
        end if;
      end if;
    end if;
    return Next;
  end Run;

  ---------
  -- Run --
  ---------

  procedure Run (State : in out State_Type) is
    Next : State_Type := Final_State;
  begin
    if State.Id in Id_Type then
      if State.On_Entry.all then
        if State.On_Execute.all then
          Next := State.On_Exit.all;
          Run (Next);
        end if;
      end if;
    end if;
  end Run;

  -----------------
  -- Final_State --
  -----------------

  function Final_State return State_Type is
    Fin : State_Type := Create;
  begin
    Fin.Id := 0;
    return Fin;
  end Final_State;

  ----------------
  -- Null_Entry --
  ----------------

  function Null_Entry return Boolean is
  begin
    return True;
  end Null_Entry;

  ------------------
  -- Null_Execute --
  ------------------

  function Null_Execute return Boolean is
  begin
    return True;
  end Null_Execute;

  ---------------
  -- Null_Exit --
  ---------------

  function Null_Exit return State_Type is
  begin
    return Final_State;
  end Null_Exit;

end State;
