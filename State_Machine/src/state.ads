package State is

  type State_Type is private;

  subtype Id_Type is Natural range 1 .. 32;
  
  type Access_Entry_Type is access
    function return Boolean;
  type Access_Execute_Type is access
    function return Boolean;
  type Access_Exit_Type is access
    function return State_Type;
  
  function Create return State_Type;
  --
  -- Create a new State for the machine
  
  procedure Register_Entry (State    : in out State_Type;
                            On_Entry : in     Access_Entry_Type);
  --
  -- Register a function to run on entry of the state
  
  procedure Register_Execute (State      : in out State_Type;
                              On_Execute : in     Access_Execute_Type);
  --
  -- Register a function to run on execute of the state
  
  procedure Register_Exit (State   : in out State_Type;
                           On_Exit : in     Access_Exit_Type);
  --
  -- Register a function to run on exit, returning the next state in the machine
  
  function Run (State : State_Type) return State_Type;
  --
  -- Run the given state returning the next state
  
  procedure Run (State : in out State_Type);
  --
  -- Run the entire state machine until Final_State is returned
  
  function Final_State return State_Type;
  --
  -- Returns the final state in the machine
  
private
  
  type State_Type is record
    Id         : Natural;
    On_Entry   : Access_Entry_Type;
    On_Execute : Access_Execute_Type;
    On_Exit    : Access_Exit_Type;
  end record;

  function Null_Entry return Boolean;
  function Null_Execute return Boolean;
  function Null_Exit return State_Type;
end State;
