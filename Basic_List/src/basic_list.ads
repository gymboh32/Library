generic 
  type Element_Type is private;
package Basic_List is

  type List_Type is private; 
  type Cursor_Type is access List_Type;
  
  function Create return List_Type;
  --
  -- Creates a Basic List
  
  procedure Add (Element : in     Element_Type;
                 Cursor  : in out Cursor_Type);
  --
  -- Adds an Element to the Next Cursor position
  
  procedure Add (Element : in     Element_Type;
                 List    : in out List_Type);
  --
  -- Adds an Element to the end of the List
  
  procedure Remove (Cursor : in     Cursor_Type;
                    List   : in out List_Type);
  --
  -- Removes the Element at the Cursor position from the List

  function Cursor (List : List_Type) return Cursor_Type;
  --
  -- Returns the Cursor at the First Element of the List
  
  function This (Cursor : Cursor_Type) return Element_Type;
  --
  -- Returns the Element at the Cursor position
  
  function Prev (Cursor : Cursor_Type) return Cursor_Type;
  --
  -- Returns the Previous Element in the List at the Cursor position
  
  function Next (Cursor : Cursor_Type) return Cursor_Type;
  --
  -- Returns the Next Element in the List at the Cursor position
  
  
private
    
  type List_Type is record
    This : Element_Type;
    Prev : Cursor_Type;
    Next : Cursor_Type;
  end record;
    
end Basic_List;
