package body Basic_List is

  -----------
  -- First --
  -----------

  function First (Cursor : Cursor_Type) return Cursor_Type is
    First_Cursor : Cursor_Type := Cursor;
  begin
    while First_Cursor.Prev /= null loop
      First_Cursor := Prev (First_Cursor);
    end loop;
    return First_Cursor;
  end First;

  ----------
  -- Last --
  ----------

  function Last (Cursor : Cursor_Type) return Cursor_Type is
    Last_Cursor : Cursor_Type := Cursor;
  begin
    while Last_Cursor.Next /= null loop
      Last_Cursor := Next (Last_Cursor);
    end loop;
    return Last_Cursor;
  end Last;

  ------------
  -- Create --
  ------------

  function Create return List_Type is
    List : List_Type;
  begin
    List.Prev := null;
    List.Next := null;
    return List;
  end Create;

  ---------
  -- Add --
  ---------

  procedure Add
    (Element : in     Element_Type;
     Cursor  : in out Cursor_Type) is
    Temp_Cursor : Cursor_Type := New List_Type;
  begin
    -- Set the element
    Temp_Cursor.This := Element;
    -- Link the Previous Element to the current cursor
    Temp_Cursor.Prev := Cursor;
    -- Link the Next Element to the Next Element of the current cursor
    Temp_Cursor.Next := Cursor.Next;
    -- Link the Next Element of the current cursor to the new Element
    Cursor.Next      := Temp_Cursor;
  end Add;

  ---------
  -- Add --
  ---------

  procedure Add
    (Element : in     Element_Type;
     List    : in out List_Type) is
    New_Cursor : Cursor_Type := new List_Type;
  begin
    -- Get to the end
    New_Cursor := Last (Cursor (List));
    -- Add a new Element
    Add (Element, New_Cursor);
    -- Back to the Beginning
    New_Cursor := First (New_Cursor);
    -- Set the List to the new list
    List := New_Cursor.all;
  end Add;

  ------------
  -- Remove --
  ------------

  procedure Remove
    (Cursor : in     Cursor_Type;
     List   : in out List_Type) is
    New_Cursor : Cursor_Type := Cursor;
  begin
    if New_Cursor.Prev /= null then
      New_Cursor := New_Cursor.Prev;
    end if;
    if New_Cursor.Next /= null then
      New_Cursor.Next := New_Cursor.Next.Next;
      New_Cursor.Next.Prev := New_Cursor;
    end if;
    List := First (Cursor).all;
  end Remove;

  ------------
  -- Cursor --
  ------------

  function Cursor (List : List_Type) return Cursor_Type is
    New_Cursor : Cursor_Type := new List_Type;
  begin
    New_Cursor.all := List;
    New_Cursor := First (New_Cursor);
    return New_Cursor;
  end Cursor;

  ----------
  -- This --
  ----------

  function This (Cursor : Cursor_Type) return Element_Type is
  begin
    return Cursor.This;
  end This;

  ----------
  -- Prev --
  ----------

  function Prev (Cursor : Cursor_Type) return Cursor_Type is
  begin
    return Cursor.Prev;
  end Prev;

  ----------
  -- Next --
  ----------

  function Next (Cursor : Cursor_Type) return Cursor_Type is
  begin
    return Cursor.Next;
  end Next;

end Basic_List;
